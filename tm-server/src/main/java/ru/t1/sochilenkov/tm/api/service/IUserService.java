package ru.t1.sochilenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    User add(@Nullable User user);

    @NotNull
    List<User> findAll();

    @Nullable
    User findById(@Nullable String Id);

    @Nullable
    User findByLogin(@Nullable String Login);

    @Nullable
    User findByEmail(@Nullable String email);

    @NotNull
    User remove(@Nullable User model);

    @Nullable
    User removeById(@Nullable String Id);

    @NotNull
    User removeByLogin(@Nullable String login);

    @NotNull
    User removeByEmail(@Nullable String email);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
