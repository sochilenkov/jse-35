package ru.t1.sochilenkov.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.api.endpoint.*;
import ru.t1.sochilenkov.tm.api.repository.*;
import ru.t1.sochilenkov.tm.api.service.*;
import ru.t1.sochilenkov.tm.endpoint.*;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.repository.*;
import ru.t1.sochilenkov.tm.service.*;
import ru.t1.sochilenkov.tm.util.SystemUtil;
import ru.t1.sochilenkov.tm.model.User;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    @Getter
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @NotNull
    @Getter
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);

    @NotNull
    @Getter
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = getPropertyService().getServerHost();
        @NotNull final String port = getPropertyService().getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    private void prepareShutdown() {
        backup.stop();
        loggerService.info("** TASK MANAGER SERVER IS SHUTTING DOWN **");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull final User firstUser = userService.create("user 1", "1", "first@site.com");
        @NotNull final User secondUser = userService.create("user 2", "2", "second@site.com");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);
    }

    public void start() {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);

        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK MANAGER SERVER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
    }

}
