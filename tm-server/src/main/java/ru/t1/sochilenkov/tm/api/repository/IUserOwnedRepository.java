package ru.t1.sochilenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(@Nullable String userId);

    @NotNull
    List<M> findAll(@Nullable String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator);

    @Nullable
    M add(@Nullable String userId, @NotNull M model);

    boolean existsById(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable String userId, @Nullable Integer index);

    int getSize(@NotNull String userId);

    @Nullable
    M remove(@Nullable String userId, @Nullable M model);

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeByIndex(@Nullable String userId, @Nullable Integer index);

}
