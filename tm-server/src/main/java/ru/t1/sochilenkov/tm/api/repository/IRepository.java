package ru.t1.sochilenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<M extends AbstractModel> {

    interface IRepositoryOptional<M extends AbstractModel> {

        @NotNull
        Optional<M> findOneById(@NotNull String id);

        @NotNull
        Optional<M> findOneByIndex(@NotNull Integer index);

    }

    default IRepositoryOptional<M> optional() {
        return new IRepositoryOptional<M>() {
            @NotNull
            @Override
            public Optional<M> findOneById(@NotNull final String id) {
                return Optional.ofNullable(IRepository.this.findOneById(id));
            }

            @NotNull
            @Override
            public Optional<M> findOneByIndex(@NotNull final Integer index) {
                return Optional.ofNullable(IRepository.this.findOneByIndex(index));
            }
        };
    }

    void clear();

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator);

    @NotNull
    M add(@Nullable M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    boolean existsById(String id);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    int getSize();

    @NotNull
    M remove(@Nullable M model);

    @Nullable
    M removeById(@NotNull String id);

    @Nullable
    M removeByIndex(@NotNull Integer index);

    void removeAll(@NotNull Collection<M> collection);

}
