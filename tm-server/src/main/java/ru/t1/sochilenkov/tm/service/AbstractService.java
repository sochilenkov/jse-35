package ru.t1.sochilenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.api.repository.IRepository;
import ru.t1.sochilenkov.tm.api.service.IService;
import ru.t1.sochilenkov.tm.enumerated.Sort;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.IdEmptyException;
import ru.t1.sochilenkov.tm.exception.field.IndexIncorrectException;
import ru.t1.sochilenkov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    @SuppressWarnings({"unchecked"})
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort.getComparator());
    }

    @NotNull
    @Override
    public M add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        return repository.add(model);
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        return repository.add(models);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        return repository.set(models);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @NotNull
    @Override
    public M remove(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        return repository.remove(model);
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null) return;
        repository.removeAll(collection);
    }

}


