package ru.t1.sochilenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.IProjectRepository;
import ru.t1.sochilenkov.tm.api.repository.ITaskRepository;
import ru.t1.sochilenkov.tm.api.repository.IUserRepository;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.api.service.IUserService;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.exception.entity.UserNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.*;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.User;
import ru.t1.sochilenkov.tm.repository.ProjectRepository;
import ru.t1.sochilenkov.tm.repository.TaskRepository;
import ru.t1.sochilenkov.tm.repository.UserRepository;
import ru.t1.sochilenkov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.t1.sochilenkov.tm.constant.UserConstant.*;

@Category(UnitCategory.class)
public class UserServiceTest {

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private IPropertyService propertyService;

    @NotNull
    private IUserService userService;

    @NotNull
    private List<User> userList;

    @Before
    public void init() {
        userRepository = new UserRepository();
        taskRepository = new TaskRepository();
        projectRepository = new ProjectRepository();
        propertyService = new PropertyService();
        userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);
        userList = new ArrayList<>();
        for (int i = 1; i <= INIT_COUNT_USERS; i++) {
            @NotNull final User user = new User();
            user.setLogin("User_" + i);
            user.setEmail("User_" + i + "@test.ru");
            user.setFirstName("User_" + i);
            user.setLastName("Userovov_" + i);
            user.setLastName("Userovich_" + i);
            userService.add(user);
            userList.add(user);
        }
    }

    @Test
    public void testClearPositive() {
        Assert.assertEquals(INIT_COUNT_USERS, userService.getSize());
        userService.clear();
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull List<User> users = userService.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(userList.size(), users.size());
        for (final User user : users) {
            Assert.assertTrue(userList.contains(user));
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testAddUserNegative() {
        userService.add(NULLABLE_USER);
    }

    @Test
    public void testAddUser() {
        Assert.assertThrows(EntityNotFoundException.class, () -> userService.add(NULLABLE_USER));
        @NotNull final User user = new User();
        Assert.assertNotNull(userService.add(user));
        Assert.assertEquals(INIT_COUNT_USERS + 1, userService.getSize());
    }

    @Test
    public void testSet() {
        Assert.assertEquals(Collections.emptyList(), userService.set(Collections.emptyList()));
        Assert.assertEquals(userList.size(), userService.getSize());
        userService.clear();
        Assert.assertEquals(0, userService.getSize());
        Assert.assertNotNull(userService.set(userList));
        Assert.assertEquals(userList, userService.findAll());
    }

    @Test
    public void testAdd() {
        @NotNull final User user = new User();
        Assert.assertEquals(Collections.emptyList(), userService.add(Collections.emptyList()));
        Assert.assertEquals(userList.size(), userService.getSize());
        userService.clear();
        userService.add(user);
        userList.add(0, user);
        Assert.assertEquals(1, userService.getSize());
        Assert.assertNotNull(userService.add(userList));
        Assert.assertEquals(userList, userService.findAll());
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(userService.existsById(NULLABLE_USER_ID));
        Assert.assertFalse(userService.existsById(EMPTY_USER_ID));
        Assert.assertFalse(userService.existsById(UUID.randomUUID().toString()));
        for (final User user : userList) {
            Assert.assertTrue(userService.existsById(user.getId()));
        }
    }

    @Test
    public void testFindOneByIdPositive() {
        Assert.assertNull(userService.findOneById(NULLABLE_USER_ID));
        Assert.assertNull(userService.findOneById(EMPTY_USER_ID));
        Assert.assertNull(userService.findOneById(UUID.randomUUID().toString()));
        for (final User user : userList) {
            Assert.assertEquals(user, userService.findOneById(user.getId()));
        }
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.findOneByIndex(NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.findOneByIndex(-1));
    }

    @Test
    public void testFindOneByIndexPositive() {
        for (int i = 0; i < INIT_COUNT_USERS; i++) {
            Assert.assertEquals(userList.get(i), userService.findOneByIndex(i));
        }
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(userList.size(), userService.getSize());
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveNegative() {
        userService.remove(NULLABLE_USER);
    }

    @Test
    public void testRemove() {
        for (final User user : userList) {
            Assert.assertNotNull(userService.remove(user));
            Assert.assertFalse(userService.findAll().contains(user));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(NULLABLE_USER_ID));
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(EMPTY_USER_ID));
    }

    @Test
    public void testRemoveByIdPositive() {
        for (final User user : userList) {
            Assert.assertNotNull(userService.removeById(user.getId()));
            Assert.assertFalse(userService.findAll().contains(user));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.removeByIndex(NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.removeByIndex(-1));
    }

    @Test
    public void testRemoveByIndexPositive() {
        for (final User user : userList) {
            Assert.assertNotNull(userService.removeByIndex(0));
            Assert.assertFalse(userService.findAll().contains(user));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveAll() {
        userService.removeAll(null);
        userService.removeAll(userList);
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testCreateNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(NULLABLE_LOGIN, NULLABLE_PASSWORD));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(EMPTY_LOGIN, NULLABLE_PASSWORD));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create(userList.get(0).getLogin(), NULLABLE_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", NULLABLE_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", EMPTY_PASSWORD));

        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(NULLABLE_LOGIN, NULLABLE_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(EMPTY_LOGIN, NULLABLE_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create(userList.get(0).getLogin(), NULLABLE_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", NULLABLE_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", EMPTY_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(ExistsEmailException.class, () -> userService.create("USR", "USR", userList.get(0).getEmail()));

        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(NULLABLE_LOGIN, NULLABLE_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(EMPTY_LOGIN, NULLABLE_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create(userList.get(0).getLogin(), NULLABLE_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", NULLABLE_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", EMPTY_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(RoleEmptyException.class, () -> userService.create("USR", "USR", NULLABLE_ROLE));
    }

    @Test
    public void testCreatePositive() {
        Assert.assertNotNull(userService.create("USR", "USR", NULLABLE_EMAIL));
        Assert.assertNotNull(userService.create("USR2", "USR2", "EMAIL"));
        Assert.assertNotNull(userService.create("USR3", "USR3", Role.USUAL));
        Assert.assertEquals(INIT_COUNT_USERS + 3, userService.getSize());
    }

    @Test
    public void testUpdateUserNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.updateUser(NULLABLE_USER_ID, null, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> userService.updateUser(EMPTY_USER_ID, null, null, null));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.updateUser(UUID.randomUUID().toString(), null, null, null));
    }

    @Test
    public void testUpdateUserPositive() {
        for (final User user : userList) {
            Assert.assertNotEquals("FST", user.getFirstName());
            Assert.assertNotEquals("LST", user.getLastName());
            Assert.assertNotEquals("MID", user.getMiddleName());
            Assert.assertNotNull(userService.updateUser(user.getId(), "FST", "LST", "MID"));
            Assert.assertEquals("FST", user.getFirstName());
            Assert.assertEquals("LST", user.getLastName());
            Assert.assertEquals("MID", user.getMiddleName());
        }
    }

    @Test
    public void testSetPasswordNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(NULLABLE_USER_ID, NULLABLE_PASSWORD));
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(EMPTY_USER_ID, NULLABLE_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(UUID.randomUUID().toString(), NULLABLE_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(UUID.randomUUID().toString(), EMPTY_PASSWORD));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.setPassword(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
    }

    @Test
    public void testSetPasswordPositive() {
        for (final User user : userList) {
            Assert.assertNotNull(userService.setPassword(user.getId(), "PSW"));
            Assert.assertEquals(HashUtil.salt(propertyService, "PSW"), user.getPasswordHash());
        }
    }

    @Test
    public void testIsLoginExists() {
        Assert.assertFalse(userService.isLoginExist(NULLABLE_LOGIN));
        Assert.assertFalse(userService.isLoginExist(EMPTY_LOGIN));
        Assert.assertTrue(userService.isLoginExist(userList.get(0).getLogin()));
    }

    @Test
    public void testIsEmailExists() {
        Assert.assertFalse(userService.isEmailExist(NULLABLE_EMAIL));
        Assert.assertFalse(userService.isEmailExist(EMPTY_EMAIL));
        Assert.assertTrue(userService.isEmailExist(userList.get(0).getEmail()));
    }

    @Test
    public void testRemoveByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(NULLABLE_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(EMPTY_LOGIN));
    }

    @Test
    public void testRemoveByLoginPositive() {
        for (final User user : userList) {
            Assert.assertNotNull(userService.removeByLogin(user.getLogin()));
            Assert.assertFalse(userService.findAll().contains(user));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveByEmailNegative() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(NULLABLE_EMAIL));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(EMPTY_EMAIL));
    }

    @Test
    public void testRemoveByEmailPositive() {
        for (final User user : userList) {
            Assert.assertNotNull(userService.removeByEmail(user.getEmail()));
            Assert.assertFalse(userService.findAll().contains(user));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testFindByEmailNegative() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(NULLABLE_EMAIL));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(EMPTY_EMAIL));
    }

    @Test
    public void testFindByEmailPositive() {
        for (final User user : userList) {
            Assert.assertEquals(user, userService.findByEmail(user.getEmail()));
        }
    }

    @Test
    public void testFindByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(NULLABLE_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(EMPTY_LOGIN));
    }

    @Test
    public void testFindByLoginPositive() {
        for (final User user : userList) {
            Assert.assertEquals(user, userService.findByLogin(user.getLogin()));
        }
    }

    @Test
    public void testFindByIdNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.findById(NULLABLE_USER_ID));
        Assert.assertThrows(IdEmptyException.class, () -> userService.findById(EMPTY_USER_ID));
    }

    @Test
    public void testFindByIdPositive() {
        for (final User user : userList) {
            Assert.assertEquals(user, userService.findById(user.getId()));
        }
    }

    @Test
    public void testLockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(NULLABLE_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(EMPTY_LOGIN));
    }

    @Test
    public void testLockUserByLoginPositive() {
        userService.lockUserByLogin(UUID.randomUUID().toString());
        for (final User user : userList) {
            Assert.assertFalse(user.getLocked());
            userService.lockUserByLogin(user.getLogin());
            Assert.assertTrue(user.getLocked());
        }
    }

    @Test
    public void testUnlockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(NULLABLE_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(EMPTY_LOGIN));
    }

    @Test
    public void testUnlockUserByLoginPositive() {
        userService.unlockUserByLogin(UUID.randomUUID().toString());
        testLockUserByLoginPositive();
        for (final User user : userList) {
            Assert.assertTrue(user.getLocked());
            userService.unlockUserByLogin(user.getLogin());
            Assert.assertFalse(user.getLocked());
        }
    }

}
