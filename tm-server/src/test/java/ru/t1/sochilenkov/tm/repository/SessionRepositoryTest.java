package ru.t1.sochilenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.ISessionRepository;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.Session;

import java.util.*;

import static ru.t1.sochilenkov.tm.constant.SessionConstant.*;

@Category(UnitCategory.class)
public class SessionRepositoryTest {

    @NotNull
    private ISessionRepository repository;

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private List<String> userIdList = new ArrayList<>();

    @Before
    public void init() {
        repository = new SessionRepository();
        sessionList = new ArrayList<>();
        userIdList = new ArrayList<>();
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++)
            userIdList.add(UUID.randomUUID().toString());
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++) {
            @NotNull final Session session = new Session();
            repository.add(userIdList.get(i), session);
            session.setUserId(userIdList.get(i));
            sessionList.add(session);
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testAddSessionNegative() {
        @Nullable Session session = null;
        repository.add(session);
    }

    @Test
    public void testAddSessionPositive() {
        @NotNull Session session = new Session();
        Assert.assertNull(repository.add(null, session));
        Assert.assertNotNull(repository.add(userIdList.get(0), session));
    }

    @Test
    public void testClear() {
        for (@NotNull final String userId : userIdList) {
            Assert.assertEquals(1, repository.getSize(userId));
            repository.clear(userId);
            Assert.assertEquals(0, repository.getSize(userId));
        }
    }

    @Test
    public void testClearWOUserId() {
        Assert.assertEquals(INIT_COUNT_SESSIONS, repository.getSize());
        repository.clear();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testFindById() {
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        for (@NotNull final String userId : userIdList) {
            Assert.assertNull(repository.findOneById(userId, UUID.randomUUID().toString()));
        }
        for (@NotNull final Session session : sessionList) {
            final Session foundSessionWOUser = repository.findOneById(session.getId());
            final Session foundSession = repository.findOneById(session.getUserId(), session.getId());
            Assert.assertNotNull(foundSessionWOUser);
            Assert.assertNotNull(foundSession);
            Assert.assertEquals(session, foundSessionWOUser);
            Assert.assertEquals(session, foundSession);
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        for (@NotNull final String userId : userIdList) {
            Assert.assertFalse(repository.existsById(userId, UUID.randomUUID().toString()));
        }
        for (@NotNull final Session session : sessionList) {
            Assert.assertTrue(repository.existsById(session.getId()));
            Assert.assertTrue(repository.existsById(session.getUserId(), session.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        Assert.assertNull(repository.findOneByIndex(9999));
        for (@NotNull final Session session : sessionList) {
            Assert.assertNull(repository.findOneByIndex(session.getUserId(), 9999));
            final Session foundSession = repository.findOneByIndex(session.getUserId(), 0);
            Assert.assertNotNull(foundSession);
            Assert.assertEquals(session, foundSession);
        }
        for (@NotNull final Session session : sessionList) {
            final Session foundSessionWOUser = repository.findOneByIndex(sessionList.indexOf(session));
            Assert.assertNotNull(foundSessionWOUser);
            Assert.assertEquals(sessionList.get(sessionList.indexOf(session)), foundSessionWOUser);
        }
    }

    @Test
    public void testFindAll() {
        List<Session> sessions = repository.findAll();
        Assert.assertNotNull(sessions);
        Assert.assertEquals(sessionList.size(), sessions.size());
        for (@NotNull final Session session : sessionList) {
            Assert.assertTrue(sessions.contains(session));
        }
        for (@NotNull final String userId : userIdList) {
            sessions = repository.findAll(userId);
            Assert.assertNotNull(sessions);
            Assert.assertEquals(1, sessions.size());
            for (@NotNull final Session session : sessionList) {
                if (session.getUserId().equals(userId))
                    Assert.assertTrue(sessions.contains(session));
            }
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveByIdWOUserNegative() {
        repository.removeById(UUID.randomUUID().toString());
    }

    @Test
    public void testRemoveByIdWOUserPositive() {
        Assert.assertEquals(INIT_COUNT_SESSIONS, repository.getSize());
        for (@NotNull final Session session : sessionList) {
            Assert.assertNotNull(repository.removeById(session.getId()));
            Assert.assertNull(repository.findOneById(session.getId()));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemoveById() {
        Assert.assertNull(repository.removeById(null, null));
        for (@NotNull final Session session : sessionList) {
            Assert.assertEquals(1, repository.getSize(session.getUserId()));
            Assert.assertNull(repository.removeById(null, session.getId()));
            Assert.assertNull(repository.removeById(session.getUserId(), null));
            Assert.assertNull(repository.removeById(session.getUserId(), UUID.randomUUID().toString()));
            Assert.assertNotNull(repository.removeById(session.getUserId(), session.getId()));
            Assert.assertNull(repository.findOneById(session.getUserId(), session.getId()));
            Assert.assertEquals(0, repository.getSize(session.getUserId()));
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveByIndexWOUserNegative() {
        repository.removeByIndex(9999);
    }

    @Test
    public void testRemoveByIndexWOUserPositive() {
        Assert.assertEquals(INIT_COUNT_SESSIONS, repository.getSize());
        for (@NotNull final Session session : sessionList) {
            Assert.assertNotNull(repository.removeByIndex(0));
            Assert.assertEquals(INIT_COUNT_SESSIONS - sessionList.indexOf(session) - 1, repository.getSize());
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemoveByIndex() {
        for (@NotNull final Session session : sessionList) {
            Assert.assertEquals(1, repository.getSize(session.getUserId()));
            Assert.assertNull(repository.removeByIndex(session.getUserId(), 9999));
            Assert.assertNotNull(repository.removeByIndex(session.getUserId(), 0));
            Assert.assertEquals(0, repository.getSize(session.getUserId()));
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveWOUserNegative() {
        repository.remove(null);
    }

    @Test
    public void testRemoveWOUserPositive() {
        Assert.assertEquals(INIT_COUNT_SESSIONS, repository.getSize());
        for (final Session session : sessionList) {
            Assert.assertNotNull(repository.remove(session));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemove() {
        for (final Session session : sessionList) {
            Assert.assertEquals(1, repository.getSize(session.getUserId()));
            Assert.assertNull(repository.remove(null, session));
            Assert.assertNull(repository.remove(session.getUserId(), null));
            Assert.assertNotNull(repository.remove(session.getUserId(), session));
            Assert.assertEquals(0, repository.getSize(session.getUserId()));
        }
    }

    @Test
    public void testSet() {
        Assert.assertNotNull(repository.set(Arrays.asList(new Session(), new Session(), new Session())));
        Assert.assertEquals(3, repository.getSize());
    }

}
