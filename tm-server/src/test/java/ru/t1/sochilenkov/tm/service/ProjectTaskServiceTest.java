package ru.t1.sochilenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.IProjectRepository;
import ru.t1.sochilenkov.tm.api.repository.ITaskRepository;
import ru.t1.sochilenkov.tm.api.service.IProjectTaskService;
import ru.t1.sochilenkov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sochilenkov.tm.exception.entity.TaskNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.sochilenkov.tm.exception.field.TaskIdEmptyException;
import ru.t1.sochilenkov.tm.exception.field.UserIdEmptyException;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.model.Task;
import ru.t1.sochilenkov.tm.repository.ProjectRepository;
import ru.t1.sochilenkov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.t1.sochilenkov.tm.constant.ProjectTaskConstant.*;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IProjectTaskService projectTaskService;

    @NotNull
    private List<Project> projectList;

    @NotNull
    private List<Task> taskList;

    @Before
    public void init() {
        projectRepository = new ProjectRepository();
        taskRepository = new TaskRepository();
        projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        for (int i = 1; i <= 2; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project");
            project.setDescription("Description");
            project.setUserId(i == 1 ? USER_ID_1 : USER_ID_2);
            projectRepository.add(project);
            projectList.add(project);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUserId(USER_ID_1);
            taskRepository.add(task);
            taskList.add(task);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUserId(USER_ID_2);
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void testBindTaskToProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(NULLABLE_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(EMPTY_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, EMPTY_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, UUID.randomUUID().toString(), NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, UUID.randomUUID().toString(), EMPTY_TASK_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.bindTaskToProject(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, projectRepository.findOneByIndex(USER_ID_1, 0).getId(), UUID.randomUUID().toString()));
    }

    @Test
    public void testBindTaskToProjectPositive() {
        for (final Project project : projectList) {
            Assert.assertEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            for (final Task task : taskList) {
                if (project.getUserId().equals(task.getUserId()))
                    Assert.assertNotNull(projectTaskService.bindTaskToProject(project.getUserId(), project.getId(), task.getId()));
            }
            Assert.assertEquals(taskRepository.findAll(project.getUserId()), taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void testUnbindTaskFromProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(NULLABLE_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(EMPTY_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, EMPTY_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, UUID.randomUUID().toString(), NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, UUID.randomUUID().toString(), EMPTY_TASK_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, projectRepository.findOneByIndex(USER_ID_1, 0).getId(), UUID.randomUUID().toString()));
    }

    @Test
    public void TestUnbindTaskFromProjectPositive() {
        testBindTaskToProjectPositive();
        for (final Project project : projectList) {
            Assert.assertEquals(taskRepository.findAll(project.getUserId()), taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            for (final Task task : taskList) {
                if (project.getUserId().equals(task.getUserId()))
                    Assert.assertNotNull(projectTaskService.unbindTaskFromProject(project.getUserId(), project.getId(), task.getId()));
            }
            Assert.assertEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void testRemoveProjectByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(NULLABLE_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(EMPTY_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(USER_ID_1, NULLABLE_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(USER_ID_1, EMPTY_PROJECT_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.removeProjectById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
    }

    @Test
    public void testRemoveProjectByIdPositive() {
        testBindTaskToProjectPositive();
        for (final Project project : projectList) {
            Assert.assertTrue(projectRepository.findAll(project.getUserId()).contains(project));
            Assert.assertEquals(INIT_COUNT_TASKS, taskRepository.findAll(project.getUserId()).size());
            projectTaskService.removeProjectById(project.getUserId(), project.getId());
            Assert.assertFalse(projectRepository.findAll(project.getUserId()).contains(project));
            Assert.assertEquals(0, taskRepository.findAll(project.getUserId()).size());
        }
        Assert.assertEquals(0, taskRepository.getSize());
    }

}
