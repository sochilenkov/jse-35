package ru.t1.sochilenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.IProjectRepository;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.Project;

import java.util.*;

import static ru.t1.sochilenkov.tm.constant.ProjectConstant.*;

@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    private IProjectRepository repository;

    @NotNull
    private List<Project> projectList;

    @Before
    public void init() {
        repository = new ProjectRepository();
        projectList = new ArrayList<>();
        for (int i = 1; i <= INIT_COUNT_PROJECTS; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project_" + i);
            project.setDescription("Description_" + i);
            repository.add(USER_ID_1, project);
            project.setUserId(USER_ID_1);
            projectList.add(project);
        }
        for (int i = 1; i <= INIT_COUNT_PROJECTS; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project_" + i);
            project.setDescription("Description_" + i);
            repository.add(USER_ID_2, project);
            project.setUserId(USER_ID_2);
            projectList.add(project);
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testAddProjectNegative() {
        repository.add(NULLABLE_PROJECT);
    }

    @Test
    public void testAddProjectPositive() {
        Project project = new Project();
        project.setName("ProjectAddTest");
        project.setDescription("ProjectAddTest desc");
        Assert.assertNull(repository.add(NULLABLE_USER_ID, project));
        Assert.assertNotNull(repository.add(USER_ID_1, project));
    }

    @Test
    public void testClear() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_1));
        repository.clear(USER_ID_1);
        Assert.assertEquals(0, repository.getSize(USER_ID_1));

        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_2));
        repository.clear(USER_ID_2);
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testClearAll() {
        Assert.assertEquals(10, repository.getSize());
        repository.clear();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testFindById() {
        Assert.assertNull(repository.findOneById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        for (@NotNull final Project project : projectList) {
            final Project foundProjectWOUser = repository.findOneById(project.getId());
            final Project foundProject = repository.findOneById(project.getUserId(), project.getId());
            Assert.assertNotNull(foundProjectWOUser);
            Assert.assertNotNull(foundProject);
            Assert.assertEquals(project, foundProjectWOUser);
            Assert.assertEquals(project, foundProject);
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        for (@NotNull final Project project : projectList) {
            Assert.assertTrue(repository.existsById(project.getId()));
            Assert.assertTrue(repository.existsById(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        Assert.assertNull(repository.findOneByIndex(USER_ID_1, 9999));
        Assert.assertNull(repository.findOneByIndex(USER_ID_2, 9999));
        Assert.assertNull(repository.findOneByIndex(9999));
        for (int i = 0; i < INIT_COUNT_PROJECTS * 2; i++) {
            final Project foundProjectWOUser = repository.findOneByIndex(projectList.indexOf(projectList.get(i)));
            Assert.assertNotNull(foundProjectWOUser);
            Assert.assertEquals(projectList.get(i), foundProjectWOUser);
        }
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            final Project foundProject = repository.findOneByIndex(USER_ID_1, i);
            Assert.assertNotNull(foundProject);
            Assert.assertEquals(projectList.get(i), foundProject);
        }
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            final Project foundProject = repository.findOneByIndex(USER_ID_2, i);
            Assert.assertNotNull(foundProject);
            Assert.assertEquals(projectList.get(i + 5), foundProject);
        }
    }

    @Test
    public void testFindAll() {
        List<Project> projects = repository.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectList.size(), projects.size());
        for (int i = 0; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertTrue(projects.contains(projectList.get(i)));
        }
        projects = repository.findAll(USER_ID_1);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertTrue(projects.contains(projectList.get(i)));
        }
        projects = repository.findAll(USER_ID_2);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertTrue(projects.contains(projectList.get(i)));
        }
    }

    @Test
    public void testFindAllComparator() {
        List<Project> projects = repository.findAll(PROJECT_COMPARATOR);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectList.size(), projects.size());
        for (int i = 0; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertTrue(projects.contains(projectList.get(i)));
        }
        projects = repository.findAll(USER_ID_1, PROJECT_COMPARATOR);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertTrue(projects.contains(projectList.get(i)));
        }
        projects = repository.findAll(USER_ID_2, PROJECT_COMPARATOR);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertTrue(projects.contains(projectList.get(i)));
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveByIdWOUserNegative() {
        repository.removeById(UUID.randomUUID().toString());
    }

    @Test
    public void testRemoveByIdWOUserPositive() {
        Assert.assertEquals(INIT_COUNT_PROJECTS * 2, repository.getSize());
        for (int i = 0; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertNotNull(repository.removeById(projectList.get(i).getId()));
            Assert.assertNull(repository.findOneById(projectList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_1));
        Assert.assertNull(repository.removeById(NULLABLE_USER_ID, NULLABLE_PROJECT_ID));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertNull(repository.removeById(NULLABLE_USER_ID, projectList.get(i).getId()));
            Assert.assertNull(repository.removeById(USER_ID_1, NULLABLE_PROJECT_ID));
            Assert.assertNull(repository.removeById(USER_ID_1, UUID.randomUUID().toString()));
            Assert.assertNotNull(repository.removeById(USER_ID_1, projectList.get(i).getId()));
            Assert.assertNull(repository.findOneById(USER_ID_1, projectList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertNull(repository.removeById(NULLABLE_USER_ID, projectList.get(i).getId()));
            Assert.assertNull(repository.removeById(USER_ID_2, NULLABLE_PROJECT_ID));
            Assert.assertNull(repository.removeById(USER_ID_2, UUID.randomUUID().toString()));
            Assert.assertNotNull(repository.removeById(USER_ID_2, projectList.get(i).getId()));
            Assert.assertNull(repository.findOneById(USER_ID_2, projectList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveByIndexWOUserNegative() {
        repository.removeByIndex(9999);
    }

    @Test
    public void testRemoveByIndexWOUserPositive() {
        Assert.assertEquals(INIT_COUNT_PROJECTS * 2, repository.getSize());
        for (int i = 0; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertNotNull(repository.removeByIndex(0));
            Assert.assertEquals(INIT_COUNT_PROJECTS * 2 - i - 1, repository.getSize());
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemoveByIndex() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertNull(repository.removeByIndex(USER_ID_1, 9999));
            Assert.assertNotNull(repository.removeByIndex(USER_ID_1, 0));
            Assert.assertEquals(INIT_COUNT_PROJECTS - i - 1, repository.getSize(USER_ID_1));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_2));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertNull(repository.removeByIndex(USER_ID_2, 9999));
            Assert.assertNotNull(repository.removeByIndex(USER_ID_2, 0));
            Assert.assertEquals(INIT_COUNT_PROJECTS - i - 1, repository.getSize(USER_ID_2));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveWOUserNegative() {
        repository.remove(NULLABLE_PROJECT);
    }

    @Test
    public void testRemoveWOUserPositive() {
        Assert.assertEquals(INIT_COUNT_PROJECTS * 2, repository.getSize());
        for (final Project project : projectList) {
            Assert.assertNotNull(repository.remove(project));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_1));
        Assert.assertNull(repository.remove(null, NULLABLE_PROJECT));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertNull(repository.remove(null, projectList.get(i)));
            Assert.assertNull(repository.remove(USER_ID_1, NULLABLE_PROJECT));
            Assert.assertNotNull(repository.remove(USER_ID_1, projectList.get(i)));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertNull(repository.remove(null, projectList.get(i)));
            Assert.assertNull(repository.remove(USER_ID_2, NULLABLE_PROJECT));
            Assert.assertNotNull(repository.remove(USER_ID_2, projectList.get(i)));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testSet() {
        Assert.assertNotNull(repository.set(Arrays.asList(new Project(), new Project(), new Project())));
        Assert.assertEquals(3, repository.getSize());
    }

}
