package ru.t1.sochilenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.component.Bootstrap;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.model.Task;
import ru.t1.sochilenkov.tm.model.User;

import java.io.File;
import java.nio.file.Files;
import java.util.Collections;
import java.util.List;

@Category(UnitCategory.class)
public class DomainServiceTest {

    @NotNull
    private static Bootstrap bootstrap;

    @NotNull
    private static DomainService domainService;

    @NotNull
    private final List<Project> projects = Collections.singletonList(new Project());

    @NotNull
    private final List<Task> tasks = Collections.singletonList(new Task());

    @NotNull
    private final List<User> users = Collections.singletonList(new User());

    @BeforeClass
    public static void startServer() {
        System.setProperty("server.port", "8081");
        bootstrap = new Bootstrap();
        domainService = new DomainService(bootstrap);
    }

    @Before
    public void init() {
        bootstrap.getProjectService().add(projects.get(0));
        bootstrap.getTaskService().add(tasks.get(0));
        bootstrap.getUserService().add(users.get(0));
    }

    @SneakyThrows
    @After
    public void deleteFiles() {
        bootstrap.getProjectService().clear();
        bootstrap.getTaskService().clear();
        bootstrap.getUserService().clear();
        Files.deleteIfExists(new File(DomainService.FILE_BACKUP).toPath());
        Files.deleteIfExists(new File(DomainService.FILE_BASE64).toPath());
        Files.deleteIfExists(new File(DomainService.FILE_BINARY).toPath());
        Files.deleteIfExists(new File(DomainService.FILE_JSON).toPath());
        Files.deleteIfExists(new File(DomainService.FILE_XML).toPath());
        Files.deleteIfExists(new File(DomainService.FILE_YAML).toPath());
    }

    @Test
    public void testSaveDataBackup() {
        Assert.assertFalse(Files.exists(new File(DomainService.FILE_BACKUP).toPath()));
        domainService.saveDataBackup();
        Assert.assertTrue(Files.exists(new File(DomainService.FILE_BACKUP).toPath()));
    }

    @Test
    public void testLoadDataBackup() {
        domainService.saveDataBackup();
        bootstrap.getProjectService().clear();
        bootstrap.getTaskService().clear();
        bootstrap.getUserService().clear();
        Assert.assertTrue(Files.exists(new File(DomainService.FILE_BACKUP).toPath()));
        Assert.assertEquals(Collections.emptyList(), bootstrap.getProjectService().findAll());
        Assert.assertEquals(Collections.emptyList(), bootstrap.getTaskService().findAll());
        Assert.assertEquals(Collections.emptyList(), bootstrap.getUserService().findAll());
        domainService.loadDataBackup();
        Assert.assertEquals(projects.get(0).getId(), bootstrap.getProjectService().findAll().get(0).getId());
        Assert.assertEquals(tasks.get(0).getId(), bootstrap.getTaskService().findAll().get(0).getId());
        Assert.assertEquals(users.get(0).getId(), bootstrap.getUserService().findAll().get(0).getId());
    }

    @Test
    public void testSaveDataBase64() {
        Assert.assertFalse(Files.exists(new File(DomainService.FILE_BASE64).toPath()));
        domainService.saveDataBase64();
        Assert.assertTrue(Files.exists(new File(DomainService.FILE_BASE64).toPath()));
    }

    @Test
    public void testLoadDataBase64() {
        domainService.saveDataBase64();
        bootstrap.getProjectService().clear();
        bootstrap.getTaskService().clear();
        bootstrap.getUserService().clear();
        Assert.assertTrue(Files.exists(new File(DomainService.FILE_BASE64).toPath()));
        Assert.assertEquals(Collections.emptyList(), bootstrap.getProjectService().findAll());
        Assert.assertEquals(Collections.emptyList(), bootstrap.getTaskService().findAll());
        Assert.assertEquals(Collections.emptyList(), bootstrap.getUserService().findAll());
        domainService.loadDataBase64();
        Assert.assertEquals(projects.get(0).getId(), bootstrap.getProjectService().findAll().get(0).getId());
        Assert.assertEquals(tasks.get(0).getId(), bootstrap.getTaskService().findAll().get(0).getId());
        Assert.assertEquals(users.get(0).getId(), bootstrap.getUserService().findAll().get(0).getId());
    }

    @Test
    public void testSaveDataBinary() {
        Assert.assertFalse(Files.exists(new File(DomainService.FILE_BINARY).toPath()));
        domainService.saveDataBinary();
        Assert.assertTrue(Files.exists(new File(DomainService.FILE_BINARY).toPath()));
    }

    @Test
    public void testLoadDataBinary() {
        domainService.saveDataBinary();
        bootstrap.getProjectService().clear();
        bootstrap.getTaskService().clear();
        bootstrap.getUserService().clear();
        Assert.assertTrue(Files.exists(new File(DomainService.FILE_BINARY).toPath()));
        Assert.assertEquals(Collections.emptyList(), bootstrap.getProjectService().findAll());
        Assert.assertEquals(Collections.emptyList(), bootstrap.getTaskService().findAll());
        Assert.assertEquals(Collections.emptyList(), bootstrap.getUserService().findAll());
        domainService.loadDataBinary();
        Assert.assertEquals(projects.get(0).getId(), bootstrap.getProjectService().findAll().get(0).getId());
        Assert.assertEquals(tasks.get(0).getId(), bootstrap.getTaskService().findAll().get(0).getId());
        Assert.assertEquals(users.get(0).getId(), bootstrap.getUserService().findAll().get(0).getId());
    }

    @Test
    public void testSaveDataJsonFasterXml() {
        Assert.assertFalse(Files.exists(new File(DomainService.FILE_JSON).toPath()));
        domainService.saveDataJsonFasterXml();
        Assert.assertTrue(Files.exists(new File(DomainService.FILE_JSON).toPath()));
    }

    @Test
    public void testLoadDataJsonFasterXml() {
        domainService.saveDataJsonFasterXml();
        bootstrap.getProjectService().clear();
        bootstrap.getTaskService().clear();
        bootstrap.getUserService().clear();
        Assert.assertTrue(Files.exists(new File(DomainService.FILE_JSON).toPath()));
        Assert.assertEquals(Collections.emptyList(), bootstrap.getProjectService().findAll());
        Assert.assertEquals(Collections.emptyList(), bootstrap.getTaskService().findAll());
        Assert.assertEquals(Collections.emptyList(), bootstrap.getUserService().findAll());
        domainService.loadDataJsonFasterXml();
        Assert.assertEquals(projects.get(0).getId(), bootstrap.getProjectService().findAll().get(0).getId());
        Assert.assertEquals(tasks.get(0).getId(), bootstrap.getTaskService().findAll().get(0).getId());
        Assert.assertEquals(users.get(0).getId(), bootstrap.getUserService().findAll().get(0).getId());
    }

    @Test
    public void testSaveDataJsonJaxB() {
        Assert.assertFalse(Files.exists(new File(DomainService.FILE_JSON).toPath()));
        domainService.saveDataJsonJaxB();
        Assert.assertTrue(Files.exists(new File(DomainService.FILE_JSON).toPath()));
    }

    @Test
    public void testLoadDataJsonJaxB() {
        domainService.saveDataJsonJaxB();
        bootstrap.getProjectService().clear();
        bootstrap.getTaskService().clear();
        bootstrap.getUserService().clear();
        Assert.assertTrue(Files.exists(new File(DomainService.FILE_JSON).toPath()));
        Assert.assertEquals(Collections.emptyList(), bootstrap.getProjectService().findAll());
        Assert.assertEquals(Collections.emptyList(), bootstrap.getTaskService().findAll());
        Assert.assertEquals(Collections.emptyList(), bootstrap.getUserService().findAll());
        domainService.loadDataJsonJaxB();
        Assert.assertEquals(projects.get(0).getId(), bootstrap.getProjectService().findAll().get(0).getId());
        Assert.assertEquals(tasks.get(0).getId(), bootstrap.getTaskService().findAll().get(0).getId());
        Assert.assertEquals(users.get(0).getId(), bootstrap.getUserService().findAll().get(0).getId());
    }

    @Test
    public void testSaveDataXmlFasterXml() {
        Assert.assertFalse(Files.exists(new File(DomainService.FILE_XML).toPath()));
        domainService.saveDataXmlFasterXml();
        Assert.assertTrue(Files.exists(new File(DomainService.FILE_XML).toPath()));
    }

    @Test
    public void testLoadDataXmlFasterXml() {
        domainService.saveDataXmlFasterXml();
        bootstrap.getProjectService().clear();
        bootstrap.getTaskService().clear();
        bootstrap.getUserService().clear();
        Assert.assertTrue(Files.exists(new File(DomainService.FILE_XML).toPath()));
        Assert.assertEquals(Collections.emptyList(), bootstrap.getProjectService().findAll());
        Assert.assertEquals(Collections.emptyList(), bootstrap.getTaskService().findAll());
        Assert.assertEquals(Collections.emptyList(), bootstrap.getUserService().findAll());
        domainService.loadDataXmlFasterXml();
        Assert.assertEquals(projects.get(0).getId(), bootstrap.getProjectService().findAll().get(0).getId());
        Assert.assertEquals(tasks.get(0).getId(), bootstrap.getTaskService().findAll().get(0).getId());
        Assert.assertEquals(users.get(0).getId(), bootstrap.getUserService().findAll().get(0).getId());
    }

    @Test
    public void testSaveDataXmlJaxB() {
        Assert.assertFalse(Files.exists(new File(DomainService.FILE_XML).toPath()));
        domainService.saveDataXmlJaxB();
        Assert.assertTrue(Files.exists(new File(DomainService.FILE_XML).toPath()));
    }

    @Test
    public void testLoadDataXmlJaxB() {
        domainService.saveDataXmlJaxB();
        bootstrap.getProjectService().clear();
        bootstrap.getTaskService().clear();
        bootstrap.getUserService().clear();
        Assert.assertTrue(Files.exists(new File(DomainService.FILE_XML).toPath()));
        Assert.assertEquals(Collections.emptyList(), bootstrap.getProjectService().findAll());
        Assert.assertEquals(Collections.emptyList(), bootstrap.getTaskService().findAll());
        Assert.assertEquals(Collections.emptyList(), bootstrap.getUserService().findAll());
        domainService.loadDataXmlJaxB();
        Assert.assertEquals(projects.get(0).getId(), bootstrap.getProjectService().findAll().get(0).getId());
        Assert.assertEquals(tasks.get(0).getId(), bootstrap.getTaskService().findAll().get(0).getId());
        Assert.assertEquals(users.get(0).getId(), bootstrap.getUserService().findAll().get(0).getId());
    }

    @Test
    public void testSaveDataYamlFasterXml() {
        Assert.assertFalse(Files.exists(new File(DomainService.FILE_YAML).toPath()));
        domainService.saveDataYamlFasterXml();
        Assert.assertTrue(Files.exists(new File(DomainService.FILE_YAML).toPath()));
    }

    @Test
    public void testLoadDataYamlFasterXml() {
        domainService.saveDataYamlFasterXml();
        bootstrap.getProjectService().clear();
        bootstrap.getTaskService().clear();
        bootstrap.getUserService().clear();
        Assert.assertTrue(Files.exists(new File(DomainService.FILE_YAML).toPath()));
        Assert.assertEquals(Collections.emptyList(), bootstrap.getProjectService().findAll());
        Assert.assertEquals(Collections.emptyList(), bootstrap.getTaskService().findAll());
        Assert.assertEquals(Collections.emptyList(), bootstrap.getUserService().findAll());
        domainService.loadDataYamlFasterXml();
        Assert.assertEquals(projects.get(0).getId(), bootstrap.getProjectService().findAll().get(0).getId());
        Assert.assertEquals(tasks.get(0).getId(), bootstrap.getTaskService().findAll().get(0).getId());
        Assert.assertEquals(users.get(0).getId(), bootstrap.getUserService().findAll().get(0).getId());
    }

}
