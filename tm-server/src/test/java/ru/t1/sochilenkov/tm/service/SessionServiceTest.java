package ru.t1.sochilenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.ISessionRepository;
import ru.t1.sochilenkov.tm.api.service.ISessionService;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.*;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.Session;
import ru.t1.sochilenkov.tm.repository.SessionRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.t1.sochilenkov.tm.constant.SessionConstant.*;

@Category(UnitCategory.class)
public class SessionServiceTest {

    @NotNull
    private ISessionRepository repository;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private List<String> userIdList = new ArrayList<>();

    @Before
    public void init() {
        repository = new SessionRepository();
        sessionList = new ArrayList<>();
        sessionService = new SessionService(repository);
        userIdList = new ArrayList<>();
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++)
            userIdList.add(UUID.randomUUID().toString());
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++) {
            @NotNull final Session session = new Session();
            repository.add(userIdList.get(i), session);
            session.setUserId(userIdList.get(i));
            sessionService.add(session);
            sessionList.add(session);
        }
    }

    @Test
    public void testClearWOUserId() {
        Assert.assertEquals(INIT_COUNT_SESSIONS, sessionService.getSize());
        sessionService.clear();
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testClearPositive() {
        for (final String userId : userIdList) {
            Assert.assertEquals(1, sessionService.getSize(userId));
            sessionService.clear(userId);
            Assert.assertEquals(0, sessionService.getSize(userId));
        }
    }

    @Test
    public void testClearNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.clear(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.clear(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllWOUserId() {
        @NotNull List<Session> sessions = sessionService.findAll();
        Assert.assertNotNull(sessions);
        Assert.assertEquals(sessionList.size(), sessions.size());
        for (final Session session : sessions) {
            Assert.assertTrue(sessionList.contains(session));
        }
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findAll(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findAll(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllPositive() {
        @NotNull List<Session> sessions;
        for (final String userId : userIdList) {
            sessions = sessionService.findAll(userId);
            Assert.assertNotNull(sessions);
            for (final Session session : sessionList) {
                if (session.getUserId().equals(userId))
                    Assert.assertTrue(sessions.contains(session));
            }
        }
        sessionService.clear();
        sessions = sessionService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(Collections.emptyList(), sessions);
    }

    @Test
    public void testAddSessionNegative() {
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionService.add(NULLABLE_SESSION));
        @NotNull final Session session = new Session();
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.add(NULLABLE_USER_ID, session));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.add(EMPTY_USER_ID, session));
    }

    @Test
    public void testAddSessionPositive() {
        Assert.assertNull(sessionService.add(userIdList.get(0), NULLABLE_SESSION));
        @NotNull Session session = new Session();
        Assert.assertNotNull(sessionService.add(userIdList.get(0), session));
        Assert.assertEquals(2, sessionService.getSize(userIdList.get(0)));
        Assert.assertNotNull(sessionService.add(session));
    }

    @Test
    public void testSet() {
        Assert.assertEquals(Collections.emptyList(), sessionService.set(Collections.emptyList()));
        Assert.assertEquals(sessionList.size(), sessionService.getSize());
        sessionService.clear();
        Assert.assertEquals(0, sessionService.getSize());
        Assert.assertNotNull(sessionService.set(sessionList));
        Assert.assertEquals(sessionList, sessionService.findAll());
    }

    @Test
    public void testAdd() {
        @NotNull final Session session = new Session();
        Assert.assertEquals(Collections.emptyList(), sessionService.add(Collections.emptyList()));
        Assert.assertEquals(sessionList.size(), sessionService.getSize());
        sessionService.clear();
        sessionService.add(session);
        sessionList.add(0, session);
        Assert.assertEquals(1, sessionService.getSize());
        Assert.assertNotNull(sessionService.add(sessionList));
        Assert.assertEquals(sessionList, sessionService.findAll());
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.existsById(NULLABLE_USER_ID, sessionList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.existsById(EMPTY_USER_ID, sessionList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.existsById(UUID.randomUUID().toString(), NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.existsById(UUID.randomUUID().toString(), EMPTY_SESSION_ID));
    }

    @Test
    public void testExistsByIdPositive() {
        Assert.assertFalse(sessionService.existsById(NULLABLE_SESSION_ID));
        Assert.assertFalse(sessionService.existsById(EMPTY_SESSION_ID));
        Assert.assertFalse(sessionService.existsById(UUID.randomUUID().toString()));
        for (final Session session : sessionList) {
            Assert.assertTrue(sessionService.existsById(session.getUserId(), session.getId()));
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneById(NULLABLE_USER_ID, sessionList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneById(EMPTY_USER_ID, sessionList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.findOneById(UUID.randomUUID().toString(), NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.findOneById(UUID.randomUUID().toString(), EMPTY_SESSION_ID));
    }

    @Test
    public void testFindOneByIdPositive() {
        Assert.assertNull(sessionService.findOneById(NULLABLE_SESSION_ID));
        Assert.assertNull(sessionService.findOneById(EMPTY_SESSION_ID));
        Assert.assertNull(sessionService.findOneById(UUID.randomUUID().toString()));
        for (final Session session : sessionList) {
            Assert.assertEquals(session, sessionService.findOneById(session.getUserId(), session.getId()));
        }
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneByIndex(NULLABLE_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneByIndex(EMPTY_USER_ID, 0));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(UUID.randomUUID().toString(), NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(-1));
    }

    @Test
    public void testFindOneByIndexPositive() {
        Assert.assertEquals(sessionList.get(0), sessionService.findOneByIndex(0));
        for (final String userId : userIdList) {
            Assert.assertEquals(sessionList.get(userIdList.indexOf(userId)), sessionService.findOneByIndex(userId, 0));
        }
    }

    @Test
    public void testGetSizeNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.getSize(NULLABLE_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.getSize(EMPTY_USER_ID));
    }

    @Test
    public void testRemoveNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.remove(NULLABLE_USER_ID, sessionList.get(0)));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.remove(EMPTY_USER_ID, sessionList.get(0)));
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionService.remove(NULLABLE_SESSION));
    }

    @Test
    public void testRemoveWOUserIdPositive() {
        for (final Session session : sessionList) {
            Assert.assertNotNull(sessionService.remove(session));
            Assert.assertFalse(sessionService.findAll().contains(session));
        }
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testRemovePositive() {
        Assert.assertNull(sessionService.remove(userIdList.get(0), NULLABLE_SESSION));
        for (final Session session : sessionList) {
            Assert.assertNotNull(sessionService.remove(session.getUserId(), session));
            Assert.assertFalse(sessionService.findAll(session.getUserId()).contains(session));
        }
        for (final String userId : userIdList)
            Assert.assertEquals(0, sessionService.getSize(userId));
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeById(NULLABLE_USER_ID, NULLABLE_SESSION_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeById(EMPTY_USER_ID, NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(UUID.randomUUID().toString(), NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(UUID.randomUUID().toString(), EMPTY_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(EMPTY_SESSION_ID));
    }

    @Test
    public void testRemoveByIdWOUserIdPositive() {
        for (final Session session : sessionList) {
            Assert.assertNotNull(sessionService.removeById(session.getId()));
            Assert.assertFalse(sessionService.findAll().contains(session));
        }
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testRemoveByIdPositive() {
        for (final Session session : sessionList) {
            Assert.assertNotNull(sessionService.removeById(session.getUserId(), session.getId()));
            Assert.assertFalse(sessionService.findAll(session.getUserId()).contains(session));
            Assert.assertEquals(0, sessionService.getSize(session.getUserId()));
        }
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeByIndex(NULLABLE_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeByIndex(EMPTY_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(UUID.randomUUID().toString(), NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(-1));
    }

    @Test
    public void testRemoveByIndexWOUserIdPositive() {
        for (final Session session : sessionList) {
            Assert.assertNotNull(sessionService.removeByIndex(0));
            Assert.assertFalse(sessionService.findAll().contains(session));
        }
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testRemoveByIndexPositive() {
        for (final String userId : userIdList) {
            Assert.assertEquals(1, sessionService.getSize(userId));
            Assert.assertEquals(sessionList.get(userIdList.indexOf(userId)), sessionService.removeByIndex(userId, 0));
            Assert.assertEquals(0, sessionService.getSize(userId));
        }
    }

    @Test
    public void testRemoveAll() {
        sessionService.removeAll(null);
        sessionService.removeAll(sessionList);
        Assert.assertEquals(0, sessionService.getSize());
    }

}
