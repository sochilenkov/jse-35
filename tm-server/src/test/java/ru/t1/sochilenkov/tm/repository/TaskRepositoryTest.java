package ru.t1.sochilenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.ITaskRepository;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.model.Task;

import java.util.*;

import static ru.t1.sochilenkov.tm.constant.TaskConstant.*;

@Category(UnitCategory.class)
public class TaskRepositoryTest {

    @NotNull
    private ITaskRepository repository;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private Project project;

    @Before
    public void init() {
        repository = new TaskRepository();
        taskList = new ArrayList<>();
        project = new Project();
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setProjectId(project.getId());
            repository.add(USER_ID_1, task);
            task.setUserId(USER_ID_1);
            taskList.add(task);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            repository.add(USER_ID_2, task);
            task.setUserId(USER_ID_2);
            taskList.add(task);
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testAddTaskNegative() {
        Task task = null;
        repository.add(task);
    }

    @Test
    public void testAddTaskPositive() {
        Task task = new Task();
        task.setName("TaskAddTest");
        task.setDescription("TaskAddTest desc");
        Assert.assertNull(repository.add(NULLABLE_USER_ID, task));
        Assert.assertNotNull(repository.add(USER_ID_1, task));
    }

    @Test
    public void testClear() {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        repository.clear(USER_ID_1);
        Assert.assertEquals(0, repository.getSize(USER_ID_1));

        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        repository.clear(USER_ID_2);
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testClearAll() {
        Assert.assertEquals(INIT_COUNT_TASKS * 2, repository.getSize());
        repository.clear();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testFindById() {
        Assert.assertNull(repository.findOneById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        for (@NotNull final Task task : taskList) {
            final Task foundTaskWOUser = repository.findOneById(task.getId());
            final Task foundTask = repository.findOneById(task.getUserId(), task.getId());
            Assert.assertNotNull(foundTaskWOUser);
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(task, foundTaskWOUser);
            Assert.assertEquals(task, foundTask);
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        for (@NotNull final Task task : taskList) {
            Assert.assertTrue(repository.existsById(task.getId()));
            Assert.assertTrue(repository.existsById(task.getUserId(), task.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        Assert.assertNull(repository.findOneByIndex(USER_ID_1, 9999));
        Assert.assertNull(repository.findOneByIndex(USER_ID_2, 9999));
        Assert.assertNull(repository.findOneByIndex(9999));
        for (int i = 0; i < INIT_COUNT_TASKS * 2; i++) {
            final Task foundTaskWOUser = repository.findOneByIndex(taskList.indexOf(taskList.get(i)));
            Assert.assertNotNull(foundTaskWOUser);
            Assert.assertEquals(taskList.get(i), foundTaskWOUser);
        }
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            final Task foundTask = repository.findOneByIndex(USER_ID_1, i);
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(taskList.get(i), foundTask);
        }
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            final Task foundTask = repository.findOneByIndex(USER_ID_2, i);
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(taskList.get(i + 5), foundTask);
        }
    }

    @Test
    public void testFindAll() {
        List<Task> tasks = repository.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskList.size(), tasks.size());
        for (int i = 0; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertTrue(tasks.contains(taskList.get(i)));
        }
        tasks = repository.findAll(USER_ID_1);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertTrue(tasks.contains(taskList.get(i)));
        }
        tasks = repository.findAll(USER_ID_2);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertTrue(tasks.contains(taskList.get(i)));
        }
    }

    @Test
    public void testFindAllComparator() {
        List<Task> tasks = repository.findAll(TASK_COMPARATOR);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskList.size(), tasks.size());
        for (int i = 0; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertTrue(tasks.contains(taskList.get(i)));
        }
        tasks = repository.findAll(USER_ID_1, TASK_COMPARATOR);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertTrue(tasks.contains(taskList.get(i)));
        }
        tasks = repository.findAll(USER_ID_2, TASK_COMPARATOR);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertTrue(tasks.contains(taskList.get(i)));
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveByIdWOUserNegative() {
        repository.removeById(UUID.randomUUID().toString());
    }

    @Test
    public void testRemoveByIdWOUserPositive() {
        Assert.assertEquals(INIT_COUNT_TASKS * 2, repository.getSize());
        for (int i = 0; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertNotNull(repository.removeById(taskList.get(i).getId()));
            Assert.assertNull(repository.findOneById(taskList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        Assert.assertNull(repository.removeById(NULLABLE_USER_ID, NULLABLE_TASK_ID));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertNull(repository.removeById(NULLABLE_USER_ID, taskList.get(i).getId()));
            Assert.assertNull(repository.removeById(USER_ID_1, NULLABLE_TASK_ID));
            Assert.assertNull(repository.removeById(USER_ID_1, UUID.randomUUID().toString()));
            Assert.assertNotNull(repository.removeById(USER_ID_1, taskList.get(i).getId()));
            Assert.assertNull(repository.findOneById(USER_ID_1, taskList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertNull(repository.removeById(NULLABLE_USER_ID, taskList.get(i).getId()));
            Assert.assertNull(repository.removeById(USER_ID_2, NULLABLE_TASK_ID));
            Assert.assertNull(repository.removeById(USER_ID_2, UUID.randomUUID().toString()));
            Assert.assertNotNull(repository.removeById(USER_ID_2, taskList.get(i).getId()));
            Assert.assertNull(repository.findOneById(USER_ID_2, taskList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveByIndexWOUserNegative() {
        repository.removeByIndex(9999);
    }

    @Test
    public void testRemoveByIndexWOUserPositive() {
        Assert.assertEquals(INIT_COUNT_TASKS * 2, repository.getSize());
        for (int i = 0; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertNotNull(repository.removeByIndex(0));
            Assert.assertEquals(INIT_COUNT_TASKS * 2 - i - 1, repository.getSize());
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemoveByIndex() {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertNull(repository.removeByIndex(USER_ID_1, 9999));
            Assert.assertNotNull(repository.removeByIndex(USER_ID_1, 0));
            Assert.assertEquals(INIT_COUNT_TASKS - i - 1, repository.getSize(USER_ID_1));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertNull(repository.removeByIndex(USER_ID_2, 9999));
            Assert.assertNotNull(repository.removeByIndex(USER_ID_2, 0));
            Assert.assertEquals(INIT_COUNT_TASKS - i - 1, repository.getSize(USER_ID_2));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveWOUserNegative() {
        repository.remove(NULLABLE_TASK);
    }

    @Test
    public void testRemoveWOUserPositive() {
        Assert.assertEquals(INIT_COUNT_TASKS * 2, repository.getSize());
        for (final Task task : taskList) {
            Assert.assertNotNull(repository.remove(task));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        Assert.assertNull(repository.remove(NULLABLE_USER_ID, NULLABLE_TASK));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertNull(repository.remove(NULLABLE_USER_ID, taskList.get(i)));
            Assert.assertNull(repository.remove(USER_ID_1, NULLABLE_TASK));
            Assert.assertNotNull(repository.remove(USER_ID_1, taskList.get(i)));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertNull(repository.remove(NULLABLE_USER_ID, taskList.get(i)));
            Assert.assertNull(repository.remove(USER_ID_2, NULLABLE_TASK));
            Assert.assertNotNull(repository.remove(USER_ID_2, taskList.get(i)));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testSet() {
        Assert.assertNotNull(repository.set(Arrays.asList(new Task(), new Task(), new Task())));
        Assert.assertEquals(3, repository.getSize());
    }

    @Test
    public void testTaskFindByProjectId() {
        List<Task> tasks = repository.findAllByProjectId(USER_ID_1, project.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
    }

}
