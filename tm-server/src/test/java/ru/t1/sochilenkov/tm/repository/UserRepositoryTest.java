package ru.t1.sochilenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.IUserRepository;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.User;

import java.util.*;

import static ru.t1.sochilenkov.tm.constant.UserConstant.*;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    @NotNull
    private IUserRepository repository;

    @NotNull
    private List<User> userList;

    @Before
    public void init() {
        repository = new UserRepository();
        userList = new ArrayList<>();
        for (int i = 1; i <= INIT_COUNT_USERS; i++) {
            @NotNull final User user = new User();
            user.setLogin("User_" + i);
            user.setEmail("User_" + i + "@test.ru");
            user.setFirstName("User_" + i);
            user.setLastName("Userovov_" + i);
            user.setLastName("Userovich_" + i);
            repository.add(user);
            userList.add(user);
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testAddUserNegative() {
        User user = null;
        repository.add(user);
    }

    @Test
    public void testAddUserPositive() {
        User user = new User();
        user.setLogin("UserAddTest");
        Assert.assertNotNull(repository.add(user));
    }

    @Test
    public void testClear() {
        Assert.assertEquals(INIT_COUNT_USERS, repository.getSize());
        repository.clear();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testFindById() {
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        for (@NotNull final User user : userList) {
            final User foundUser = repository.findOneById(user.getId());
            Assert.assertNotNull(foundUser);
            Assert.assertEquals(user, foundUser);
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        for (@NotNull final User user : userList) {
            Assert.assertTrue(repository.existsById(user.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        Assert.assertNull(repository.findOneByIndex(9999));
        for (final User user : userList) {
            final User foundUser = repository.findOneByIndex(userList.indexOf(user));
            Assert.assertNotNull(foundUser);
            Assert.assertEquals(user, foundUser);
        }
    }

    @Test
    public void testFindAll() {
        List<User> users = repository.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(userList.size(), users.size());
        for (final User user : userList) {
            Assert.assertTrue(users.contains(user));
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveByIdNegative() {
        repository.removeById(UUID.randomUUID().toString());
    }

    @Test
    public void testRemoveByIdPositive() {
        Assert.assertEquals(INIT_COUNT_USERS, repository.getSize());
        for (final User user : userList) {
            Assert.assertNotNull(repository.removeById(user.getId()));
            Assert.assertNull(repository.findOneById(user.getId()));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveByIndexNegative() {
        repository.removeByIndex(9999);
    }

    @Test
    public void testRemoveByIndexPositive() {
        Assert.assertEquals(INIT_COUNT_USERS, repository.getSize());
        for (final User user : userList) {
            Assert.assertNotNull(repository.removeByIndex(0));
            Assert.assertEquals(INIT_COUNT_USERS - userList.indexOf(user) - 1, repository.getSize());
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNegative() {
        repository.remove(null);
    }

    @Test
    public void testRemovePositive() {
        Assert.assertEquals(INIT_COUNT_USERS, repository.getSize());
        for (final User user : userList) {
            Assert.assertNotNull(repository.remove(user));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testSet() {
        Assert.assertNotNull(repository.set(Arrays.asList(new User(), new User(), new User())));
        Assert.assertEquals(3, repository.getSize());
    }

    @Test
    public void testExistsLogin() {
        Assert.assertFalse(repository.isLoginExist(""));
        for (final User user : userList) {
            Assert.assertTrue(repository.isLoginExist(user.getLogin()));
        }
    }

    @Test
    public void testFindByLogin() {
        Assert.assertNull(repository.findByLogin(""));
        for (final User user : userList) {
            Assert.assertNotNull(repository.findByLogin(user.getLogin()));
        }
    }

    @Test
    public void testExistsEmail() {
        Assert.assertFalse(repository.isEmailExist(""));
        for (final User user : userList) {
            Assert.assertTrue(repository.isEmailExist(user.getEmail()));
        }
    }

    @Test
    public void testFindByEmail() {
        Assert.assertNull(repository.findByEmail(""));
        for (final User user : userList) {
            Assert.assertNotNull(repository.findByEmail(user.getEmail()));
        }
    }

}
