package ru.t1.sochilenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.IProjectRepository;
import ru.t1.sochilenkov.tm.api.service.IProjectService;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.*;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.repository.ProjectRepository;

import java.util.*;

import static ru.t1.sochilenkov.tm.constant.ProjectConstant.*;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private IProjectRepository repository;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private List<Project> projectList;

    @Before
    public void init() {
        repository = new ProjectRepository();
        projectService = new ProjectService(repository);
        projectList = new ArrayList<>();
        for (int i = 1; i <= INIT_COUNT_PROJECTS; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project_" + i);
            project.setDescription("Description_" + i);
            project.setUserId(USER_ID_1);
            projectList.add(project);
        }
        for (int i = 1; i <= INIT_COUNT_PROJECTS; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project_" + i);
            project.setDescription("Description_" + i);
            project.setUserId(USER_ID_2);
            projectList.add(project);
        }
        projectService.set(projectList);
    }

    @Test
    public void testClearWOUserId() {
        Assert.assertEquals(INIT_COUNT_PROJECTS * 2, projectService.getSize());
        projectService.clear();
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void testClearPositive() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, projectService.getSize(USER_ID_1));
        projectService.clear(USER_ID_1);
        Assert.assertEquals(0, projectService.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_PROJECTS, projectService.getSize(USER_ID_2));
        projectService.clear(USER_ID_2);
        Assert.assertEquals(0, projectService.getSize(USER_ID_2));
    }

    @Test
    public void testClearNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllWOUserId() {
        @NotNull List<Project> projects = projectService.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectList.size(), projects.size());
        for (final Project project : projects) {
            Assert.assertTrue(projectList.contains(project));
        }
    }

    @Test
    public void testFindAllWOUserIdSort() {
        @NotNull List<Project> projects = projectService.findAll(CREATED_SORT);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectList.size(), projects.size());
        for (final Project project : projects) {
            Assert.assertTrue(projectList.contains(project));
        }
        projects = projectService.findAll(NULLABLE_SORT);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectList.size(), projects.size());
        for (final Project project : projects) {
            Assert.assertTrue(projectList.contains(project));
        }
    }

    @Test
    public void testFindAllWOUserIdComparator() {
        @NotNull List<Project> projects = projectService.findAll(PROJECT_COMPARATOR);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectList.size(), projects.size());
        for (final Project project : projects) {
            Assert.assertTrue(projectList.contains(project));
        }
        projects = projectService.findAll(NULLABLE_COMPARATOR);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectList.size(), projects.size());
        for (final Project project : projects) {
            Assert.assertTrue(projectList.contains(project));
        }
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllPositive() {
        @NotNull List<Project> projects = projectService.findAll(USER_ID_1);
        Assert.assertNotNull(projects);
        for (final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                Assert.assertTrue(projects.contains(project));
        }
        projects = projectService.findAll(USER_ID_2);
        Assert.assertNotNull(projects);
        for (final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_2))
                Assert.assertTrue(projects.contains(project));
        }
        projectService.clear();
        projects = projectService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(Collections.emptyList(), projects);
    }

    @Test
    public void testFindAllNegativeComparator() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(NULLABLE_USER_ID, PROJECT_COMPARATOR));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(EMPTY_USER_ID, PROJECT_COMPARATOR));
    }

    @Test
    public void testFindAllPositiveComparator() {
        List<Project> projects = projectService.findAll(USER_ID_1, PROJECT_COMPARATOR);
        Assert.assertNotNull(projects);
        for (final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                Assert.assertTrue(projects.contains(project));
        }
        projects = projectService.findAll(USER_ID_2, NULLABLE_COMPARATOR);
        Assert.assertNotNull(projects);
        for (final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_2))
                Assert.assertTrue(projects.contains(project));
        }
    }

    @Test
    public void testFindAllSortNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(NULLABLE_USER_ID, CREATED_SORT));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(EMPTY_USER_ID, CREATED_SORT));
    }

    @Test
    public void testFindAllSortPositive() {
        List<Project> projects = projectService.findAll(USER_ID_1, CREATED_SORT);
        Assert.assertNotNull(projects);
        for (final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                Assert.assertTrue(projects.contains(project));
        }
        projects = projectService.findAll(USER_ID_2, NULLABLE_SORT);
        Assert.assertNotNull(projects);
        for (final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_2))
                Assert.assertTrue(projects.contains(project));
        }
    }

    @Test
    public void testAddProjectNegative() {
        Assert.assertThrows(EntityNotFoundException.class, () -> projectService.add(NULLABLE_PROJECT));
        @NotNull final Project project = new Project();
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add(NULLABLE_USER_ID, project));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add(EMPTY_USER_ID, project));
    }

    @Test
    public void testAddProjectPositive() {
        Assert.assertNull(projectService.add(USER_ID_1, NULLABLE_PROJECT));
        @NotNull Project project = new Project();
        project.setName("ProjectAddTest");
        project.setDescription("ProjectAddTest desc");
        Assert.assertNotNull(projectService.add(USER_ID_1, project));
        Assert.assertEquals(INIT_COUNT_PROJECTS + 1, projectService.getSize(USER_ID_1));
        Assert.assertNotNull(projectService.add(project));
    }

    @Test
    public void testSet() {
        Assert.assertEquals(Collections.emptyList(), projectService.set(Collections.emptyList()));
        Assert.assertEquals(projectList.size(), projectService.getSize());
        projectService.clear();
        Assert.assertEquals(0, projectService.getSize());
        Assert.assertNotNull(projectService.set(projectList));
        Assert.assertEquals(projectList, projectService.findAll());
    }

    @Test
    public void testAdd() {
        @NotNull final Project project = new Project();
        Assert.assertEquals(Collections.emptyList(), projectService.add(Collections.emptyList()));
        Assert.assertEquals(projectList.size(), projectService.getSize());
        projectService.clear();
        projectService.add(project);
        projectList.add(0, project);
        Assert.assertEquals(1, projectService.getSize());
        Assert.assertNotNull(projectService.add(projectList));
        Assert.assertEquals(projectList, projectService.findAll());
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.existsById(NULLABLE_USER_ID, projectList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.existsById(EMPTY_USER_ID, projectList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.existsById(USER_ID_1, NULLABLE_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.existsById(USER_ID_1, EMPTY_PROJECT_ID));
    }

    @Test
    public void testExistsByIdPositive() {
        Assert.assertFalse(projectService.existsById(NULLABLE_PROJECT_ID));
        Assert.assertFalse(projectService.existsById(EMPTY_PROJECT_ID));
        Assert.assertFalse(projectService.existsById(UUID.randomUUID().toString()));
        for (final Project project : projectList) {
            Assert.assertTrue(projectService.existsById(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById(NULLABLE_USER_ID, projectList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById(EMPTY_USER_ID, projectList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(USER_ID_1, NULLABLE_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(USER_ID_1, EMPTY_PROJECT_ID));
    }

    @Test
    public void testFindOneByIdPositive() {
        Assert.assertNull(projectService.findOneById(NULLABLE_PROJECT_ID));
        Assert.assertNull(projectService.findOneById(EMPTY_PROJECT_ID));
        Assert.assertNull(projectService.findOneById(UUID.randomUUID().toString()));
        for (final Project project : projectList) {
            Assert.assertEquals(project, projectService.findOneById(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneByIndex(NULLABLE_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneByIndex(EMPTY_USER_ID, 0));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(USER_ID_1, NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(-1));
    }

    @Test
    public void testFindOneByIndexPositive() {
        Assert.assertEquals(projectList.get(0), projectService.findOneByIndex(0));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertEquals(projectList.get(i), projectService.findOneByIndex(USER_ID_1, i));
        }
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertEquals(projectList.get(i), projectService.findOneByIndex(USER_ID_2, i - 5));
        }
    }

    @Test
    public void testGetSizeNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.getSize(NULLABLE_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.getSize(EMPTY_USER_ID));
    }

    @Test
    public void testRemoveNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.remove(NULLABLE_USER_ID, projectList.get(0)));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.remove(EMPTY_USER_ID, projectList.get(0)));
        Assert.assertThrows(EntityNotFoundException.class, () -> projectService.remove(NULLABLE_PROJECT));
    }

    @Test
    public void testRemoveWOUserIdPositive() {
        for (final Project project : projectList) {
            Assert.assertNotNull(projectService.remove(project));
            Assert.assertFalse(projectService.findAll().contains(project));
        }
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void testRemovePositive() {
        Assert.assertNull(projectService.remove(USER_ID_1, NULLABLE_PROJECT));
        for (final Project project : projectList) {
            Assert.assertNotNull(projectService.remove(project.getUserId(), project));
            Assert.assertFalse(projectService.findAll(project.getUserId()).contains(project));
        }
        Assert.assertEquals(0, projectService.getSize(USER_ID_1));
        Assert.assertEquals(0, projectService.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById(NULLABLE_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById(EMPTY_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(USER_ID_1, NULLABLE_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(USER_ID_1, EMPTY_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(NULLABLE_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(EMPTY_PROJECT_ID));
    }

    @Test
    public void testRemoveByIdWOUserIdPositive() {
        for (final Project project : projectList) {
            Assert.assertNotNull(projectService.removeById(project.getId()));
            Assert.assertFalse(projectService.findAll().contains(project));
        }
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void testRemoveByIdPositive() {
        for (final Project project : projectList) {
            Assert.assertNotNull(projectService.removeById(project.getUserId(), project.getId()));
            Assert.assertFalse(projectService.findAll(project.getUserId()).contains(project));
        }
        Assert.assertEquals(0, projectService.getSize(USER_ID_1));
        Assert.assertEquals(0, projectService.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeByIndex(NULLABLE_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeByIndex(EMPTY_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(USER_ID_1, NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(-1));
    }

    @Test
    public void testRemoveByIndexWOUserIdPositive() {
        for (final Project project : projectList) {
            Assert.assertNotNull(projectService.removeByIndex(0));
            Assert.assertFalse(projectService.findAll().contains(project));
        }
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void testRemoveByIndexPositive() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, projectService.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertEquals(projectList.get(i), projectService.removeByIndex(USER_ID_1, 0));
        }
        Assert.assertEquals(INIT_COUNT_PROJECTS, projectService.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertEquals(projectList.get(i), projectService.removeByIndex(USER_ID_2, 0));
        }
        Assert.assertEquals(0, projectService.getSize(USER_ID_1));
        Assert.assertEquals(0, projectService.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveAll() {
        projectService.removeAll(null);
        projectService.removeAll(projectList);
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void testCreateProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create(NULLABLE_USER_ID, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create(EMPTY_USER_ID, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(USER_ID_1, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(USER_ID_1, "", null));
    }

    @Test
    public void testCreateProjectPositive() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, projectService.getSize(USER_ID_1));
        Assert.assertNotNull(projectService.create(USER_ID_1, "PROJ", "PROJ_DESC"));
        Assert.assertEquals(INIT_COUNT_PROJECTS + 1, projectService.getSize(USER_ID_1));

        Assert.assertEquals(INIT_COUNT_PROJECTS, projectService.getSize(USER_ID_2));
        Assert.assertNotNull(projectService.create(USER_ID_2, "PROJ", "PROJ_DESC"));
        Assert.assertEquals(INIT_COUNT_PROJECTS + 1, projectService.getSize(USER_ID_2));

        Assert.assertEquals(INIT_COUNT_PROJECTS + 1, projectService.getSize(USER_ID_1));
        Assert.assertNotNull(projectService.create(USER_ID_1, "PROJ_2", ""));
        Assert.assertEquals(INIT_COUNT_PROJECTS + 2, projectService.getSize(USER_ID_1));

        Assert.assertEquals(INIT_COUNT_PROJECTS + 1, projectService.getSize(USER_ID_2));
        Assert.assertNotNull(projectService.create(USER_ID_2, "PROJ_2", ""));
        Assert.assertEquals(INIT_COUNT_PROJECTS + 2, projectService.getSize(USER_ID_2));

        Assert.assertEquals(INIT_COUNT_PROJECTS + 2, projectService.getSize(USER_ID_1));
        Assert.assertNotNull(projectService.create(USER_ID_1, "PROJ_3", null));
        Assert.assertEquals(INIT_COUNT_PROJECTS + 3, projectService.getSize(USER_ID_1));
    }

    @Test
    public void testChangeProjectStatusByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(NULLABLE_USER_ID, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(EMPTY_USER_ID, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(USER_ID_1, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(USER_ID_1, "", null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.changeProjectStatusById(USER_ID_1, UUID.randomUUID().toString(), null));
        Assert.assertThrows(StatusIncorrectException.class, () -> projectService.changeProjectStatusById(USER_ID_1, projectService.findOneByIndex(USER_ID_1, 0).getId(), null));
    }

    @Test
    public void testChangeProjectStatusByIdPositive() {
        for (final Project project : projectList) {
            Assert.assertNotNull(projectService.changeProjectStatusById(project.getUserId(), project.getId(), IN_PROGRESS_STATUS));
            Assert.assertEquals(project.getStatus(), projectService.findOneById(project.getUserId(), project.getId()).getStatus());
        }
    }

    @Test
    public void testChangeProjectStatusByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusByIndex(NULLABLE_USER_ID, NULLABLE_INDEX, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusByIndex(EMPTY_USER_ID, NULLABLE_INDEX, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(USER_ID_1, NULLABLE_INDEX, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(USER_ID_1, -1, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.changeProjectStatusByIndex(UUID.randomUUID().toString(), 0, null));
        Assert.assertThrows(StatusIncorrectException.class, () -> projectService.changeProjectStatusByIndex(USER_ID_1, 0, null));
    }

    @Test
    public void testChangeProjectStatusByIndexPositive() {
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertEquals(projectList.get(i), projectService.changeProjectStatusByIndex(USER_ID_1, i, IN_PROGRESS_STATUS));
        }
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertEquals(projectList.get(i), projectService.changeProjectStatusByIndex(USER_ID_2, i - 5, IN_PROGRESS_STATUS));
        }
    }

    @Test
    public void testUpdateByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(NULLABLE_USER_ID, null, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(EMPTY_USER_ID, null, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(USER_ID_1, null, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(USER_ID_1, "", null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(USER_ID_1, UUID.randomUUID().toString(), null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(USER_ID_1, UUID.randomUUID().toString(), "", null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.updateById(USER_ID_1, UUID.randomUUID().toString(), "UPD_PROJ_1", null));
    }

    @Test
    public void testUpdateByIdPositive() {
        for (final Project project : projectList) {
            Assert.assertNotNull(projectService.updateById(project.getUserId(), project.getId(), project.getName(), null));
            Assert.assertNotNull(projectService.updateById(project.getUserId(), project.getId(), project.getName(), ""));
            Assert.assertNotNull(projectService.updateById(project.getUserId(), project.getId(), project.getName() + "_upd", project.getDescription() + "_upd"));
            Assert.assertEquals(project, projectService.findOneById(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void testUpdateByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateByIndex(NULLABLE_USER_ID, null, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateByIndex(EMPTY_USER_ID, null, null, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex(USER_ID_1, null, null, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex(USER_ID_1, -1, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateByIndex(USER_ID_1, 0, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateByIndex(USER_ID_1, 0, "", null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.updateByIndex(UUID.randomUUID().toString(), 0, "UPD_PROJ_1", null));
    }

    @Test
    public void testUpdateByIndexPositive() {
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Project project = projectList.get(i);
            Assert.assertNotNull(projectService.updateByIndex(project.getUserId(), i, project.getName(), null));
            Assert.assertNotNull(projectService.updateByIndex(project.getUserId(), i, project.getName(), ""));
            Assert.assertNotNull(projectService.updateByIndex(project.getUserId(), i, project.getName() + "_upd", project.getDescription() + "_upd"));
            Assert.assertEquals(project, projectService.findOneByIndex(project.getUserId(), i));
        }
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Project project = projectList.get(i);
            Assert.assertNotNull(projectService.updateByIndex(project.getUserId(), i - 5, project.getName(), null));
            Assert.assertNotNull(projectService.updateByIndex(project.getUserId(), i - 5, project.getName(), ""));
            Assert.assertNotNull(projectService.updateByIndex(project.getUserId(), i - 5, project.getName() + "_upd", project.getDescription() + "_upd"));
            Assert.assertEquals(project, projectService.findOneByIndex(project.getUserId(), i - 5));
        }
    }

}
