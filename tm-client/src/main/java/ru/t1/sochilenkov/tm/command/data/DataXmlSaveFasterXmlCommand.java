package ru.t1.sochilenkov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.dto.request.DataXmlSaveFasterXmlRequest;

public class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save data in xml file.";

    @NotNull
    public static final String NAME = "data-save-xml";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest(getToken());
        getDomainEndpoint().saveDataXmlFasterXml(request);
    }

}
