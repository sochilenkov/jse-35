package ru.t1.sochilenkov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.dto.request.DataBackupLoadRequest;

public class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Load data from backup file.";

    @NotNull
    public static final String NAME = "backup-load";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest(getToken());
        getDomainEndpoint().loadDataBackup(request);
    }

}
