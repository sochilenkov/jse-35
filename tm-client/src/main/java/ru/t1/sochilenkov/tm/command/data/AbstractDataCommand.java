package ru.t1.sochilenkov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.sochilenkov.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    public AbstractDataCommand() {
    }

    @NotNull
    public IDomainEndpoint getDomainEndpoint() {
        return getServiceLocator().getDomainEndpoint();
    }

}
