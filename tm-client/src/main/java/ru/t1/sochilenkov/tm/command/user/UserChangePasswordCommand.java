package ru.t1.sochilenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.dto.request.UserChangePasswordRequest;
import ru.t1.sochilenkov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Change password for current user.";

    @NotNull
    public static final String NAME = "user-change-password";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();

        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setPassword(password);

        getUserEndpoint().changeUserPassword(request);
    }

}
