package ru.t1.sochilenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.dto.request.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Clear project list.";

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");

        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());

        getProjectEndpoint().clearProject(request);
    }

}
