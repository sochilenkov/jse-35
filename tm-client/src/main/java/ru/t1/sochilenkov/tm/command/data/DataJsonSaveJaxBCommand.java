package ru.t1.sochilenkov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.dto.request.DataJsonSaveJaxBRequest;

public class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save data in json file.";

    @NotNull
    public static final String NAME = "data-save-json-jaxb";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final DataJsonSaveJaxBRequest request = new DataJsonSaveJaxBRequest(getToken());
        getDomainEndpoint().saveDataJsonJaxB(request);
    }

}
