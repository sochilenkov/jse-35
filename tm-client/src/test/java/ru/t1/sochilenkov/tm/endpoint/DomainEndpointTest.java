package ru.t1.sochilenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.sochilenkov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.dto.request.*;
import ru.t1.sochilenkov.tm.marker.SoapCategory;
import ru.t1.sochilenkov.tm.service.PropertyService;

@Category(SoapCategory.class)
public class DomainEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    private String adminToken;

    @Before
    public void init() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin("admin");
        loginRequest.setPassword("admin");
        adminToken = authEndpoint.login(loginRequest).getToken();
    }

    @Test
    public void testDataBackupLoadEndpoint() {
        testDataBackupSaveEndpoint();
        @NotNull DataBackupLoadRequest dataBackupLoadRequest = new DataBackupLoadRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataBackup(dataBackupLoadRequest));
    }

    @Test
    public void testDataBackupSaveEndpoint() {
        @NotNull DataBackupSaveRequest dataBackupSaveRequest = new DataBackupSaveRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataBackup(dataBackupSaveRequest));
    }

    @Test
    public void testDataBase64LoadEndpoint() {
        testDataBase64SaveEndpoint();
        @NotNull DataBase64LoadRequest dataBackupLoadRequest = new DataBase64LoadRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataBase64(dataBackupLoadRequest));
    }

    @Test
    public void testDataBase64SaveEndpoint() {
        @NotNull DataBase64SaveRequest dataBase64SaveRequest = new DataBase64SaveRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataBase64(dataBase64SaveRequest));
    }

    @Test
    public void testDataBinaryLoadEndpoint() {
        testDataBinarySaveEndpoint();
        @NotNull DataBinaryLoadRequest dataBackupLoadRequest = new DataBinaryLoadRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataBinary(dataBackupLoadRequest));
    }

    @Test
    public void testDataBinarySaveEndpoint() {
        @NotNull DataBinarySaveRequest dataBinarySaveRequest = new DataBinarySaveRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataBinary(dataBinarySaveRequest));
    }

    @Test
    public void testDataJsonLoadFasterXmlEndpoint() {
        testDataJsonSaveFasterXmlEndpoint();
        @NotNull DataJsonLoadFasterXmlRequest jsonLoadFasterXmlRequest = new DataJsonLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataJsonFasterXml(jsonLoadFasterXmlRequest));
    }

    @Test
    public void testDataJsonLoadJaxBEndpoint() {
        testDataJsonSaveJaxBEndpoint();
        @NotNull DataJsonLoadJaxBRequest jsonLoadJaxBRequest = new DataJsonLoadJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataJsonJaxB(jsonLoadJaxBRequest));
    }

    @Test
    public void testDataJsonSaveFasterXmlEndpoint() {
        @NotNull DataJsonSaveFasterXmlRequest jsonSaveFasterXmlRequest = new DataJsonSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataJsonFasterXml(jsonSaveFasterXmlRequest));
    }

    @Test
    public void testDataJsonSaveJaxBEndpoint() {
        @NotNull DataJsonSaveJaxBRequest jsonSaveJaxBRequest = new DataJsonSaveJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataJsonJaxB(jsonSaveJaxBRequest));
    }

    @Test
    public void testDataXmlLoadFasterXmlEndpoint() {
        testDataXmlSaveFasterXmlEndpoint();
        @NotNull DataXmlLoadFasterXmlRequest xmlLoadFasterXmlRequest = new DataXmlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataXmlFasterXml(xmlLoadFasterXmlRequest));
    }

    @Test
    public void testDataXmlLoadJaxBEndpoint() {
        testDataXmlSaveFasterXmlEndpoint();
        @NotNull DataXmlLoadJaxBRequest xmlLoadJaxBRequest = new DataXmlLoadJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataXmlJaxB(xmlLoadJaxBRequest));
    }

    @Test
    public void testDataXmlSaveFasterXmlEndpoint() {
        @NotNull DataXmlSaveFasterXmlRequest xmlSaveFasterXmlRequest = new DataXmlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataXmlFasterXml(xmlSaveFasterXmlRequest));
    }

    @Test
    public void testDataXmlSaveJaxBEndpoint() {
        @NotNull DataXmlSaveJaxBRequest xmlSaveJaxBRequest = new DataXmlSaveJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataXmlJaxB(xmlSaveJaxBRequest));
    }

    @Test
    public void testDataYamlLoadFasterXmlEndpoint() {
        testDataYamlSaveFasterXmlEndpoint();
        @NotNull DataYamlLoadFasterXmlRequest yamlLoadFasterXmlRequest = new DataYamlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataYamlFasterXml(yamlLoadFasterXmlRequest));
    }

    @Test
    public void testDataYamlSaveFasterXmlEndpoint() {
        @NotNull DataYamlSaveFasterXmlRequest yamlSaveFasterXmlRequest = new DataYamlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataYamlFasterXml(yamlSaveFasterXmlRequest));
    }

}
