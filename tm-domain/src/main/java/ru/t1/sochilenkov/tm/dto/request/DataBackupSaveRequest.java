package ru.t1.sochilenkov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataBackupSaveRequest extends AbstractUserRequest {

    public DataBackupSaveRequest(@Nullable final String token) {
        super(token);
    }

}
