package ru.t1.sochilenkov.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class DataBackupLoadResponse extends AbstractResponse {
}
