package ru.t1.sochilenkov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataXmlSaveJaxBRequest extends AbstractUserRequest {

    public DataXmlSaveJaxBRequest(@Nullable final String token) {
        super(token);
    }

}
