package ru.t1.sochilenkov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.model.Task;

import java.util.List;

@NoArgsConstructor
public final class TaskListByProjectIdResponse extends AbstractTaskResponse {

    @Nullable
    @Getter
    @Setter
    private List<Task> tasks;

    public TaskListByProjectIdResponse(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

}
